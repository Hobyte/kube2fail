#! /bin/sh

# run kail and fail2ban server in parallel
parallel --lb :::\
 './kail/kail -l kail.log=true | tee ./collected.logs'\
 'fail2ban-server' 'sleep 5; tail -f /var/log/fail2ban.log'\
 'python server.py'
