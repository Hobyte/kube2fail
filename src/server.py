import os
import json
from http.server import BaseHTTPRequestHandler, HTTPServer

hostname = ''
port = 80

class BansServer(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/bans':
            bans = os.popen('fail2ban-client banned').read()
            #bans = json.loads(bans)
            self.send_response(200)
            self.send_header('Content-type', 'json')
            self.end_headers()
            self.wfile.write(bans.encode('utf-8'))
        else:
            self.send_response(404)
            self.end_headers()

if __name__ == '__main__':
    web_server = HTTPServer((hostname, port), BansServer)
    print("Server started http://%s:%s" % (hostname, port   ))
    try:
        web_server.serve_forever()
    except KeyboardInterrupt:
        pass
    web_server.server_close()
