# fail4kube

## Get Started

Install these programms:
- [k3d](https://k3d.io) for a local kubernets cluster
- [tilt](https://tilt.dev) for live builds and reloading of kubernetes files

start tilt:
```bash
tilt up
```
press *space* to open the tilt UI in the browser to see the current deployment state. There you can see all workloads with their state and logs.

## Start kubernetes cluster

[k3d](https://k3d.io) is used to run a local test cluster. To create a new cluster, run `k3d cluster create --config ./k3d-cluster.yaml`

## log collecting

[kail](https://github.com/boz/kail) is used to collect all logs from the cluster. Kail will collect logs from all **pods** marked with the label `kail.log: 'true'`

## nginx test deployment

nginx is used to test, if **fail4kube** is working. The deployment is accessible through the load balancer of k3d on [localhost:8080](http://localhost:8080). Therefore, two files are configured:
- [index.hmtl](http://localhost:8080) with unrestricted access.This will lead you to the *Normal Area*
- [admin.hmtl](http://localhost:8080/admin.html) protected by basic auth. This will open the *Admin Area*. Two users are configured to access it:
    - *user1*: Password *user1*
    - *user2*: Password *user2*

## fail2ban configuration

To configure *fail2ban*, several config files are needed. For the kubernetes deployment, all configuration files are in the two ConfigMaps [fail2ban-config](./kubernetes/fail2ban-config.yaml) and [fail2ban-filter](./kubernetes/fail2ban-filter.yaml). The first ConfigMap contains the general config file `fail2ban.local` and the jail config file `jail.local`. As the [log collection](#log-collecting) adds additional information to the logs that get analysed bz *fail2ban*, the failregex that come with *fail2ban* can't be used. All filter configuration is located in [fail2ban-filter](./kubernetes/fail2ban-filter.yaml). All of the files in these two ConfigMaps get mounted to the **fail4kube** container.

## fail2ban rest api

to access **fail2ban** from other containers, [fail2rest](https://github.com/Sean-Der/fail2rest/tree/master) is installed. It provides a REST API to **fail2ban** on port *80*. To get all bans, the endpoint [/global/bans](localhost:5000/global/bans) can be queried. Alternatively, all infos about a jail can be requested from the endpoint [/jail/<jail-name>](localhost:5000/jail/nginx-basicauth), but this endpoint only works sometimes. The access these urls from other pods in the cluster, use the url `fail4kube.fail4kube`, so the two endpoints are [fail4kube.fail4kube/global/bans](fail4kube.fail4kube/global/bans) and [fail4kube.fail4kube/global/bans](fail4kube.fail4kube/global/bans).
